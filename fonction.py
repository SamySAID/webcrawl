""""
Fonction du webcrawl
"""

from urllib.request import urlopen
import bs4 as BeautifulSoup
import urllib.error

list_title = []
list_link = []
list_keywords = []
list_description = []
dico = dict()

###	FONCTION EXTRACTION DE BASE	###
########################################
def extract_link(soup):

    try:
        for a in soup.find_all('a'):
            if str(a.get('href')).find("http") != (-1):
                list_link.append(a.get('href'))
				#print(a.get('href'))
    except UnicodeEncodeError:
        print("oups")

    dico["links"] = list_link

    return dico

def extract_data(soup):
    try:
        title = soup.find('title')
        list_title.append(title.text)
        for word in title.text.split(' '):
                list_keywords.append(word)
    except UnicodeEncodeError:
        print("oups")
    try:
        for a in soup.find_all('a'):
            if str(a.get('href')).find("http") != (-1):
                list_link.append(a.get('href'))
				#print(a.get('href'))
    except UnicodeEncodeError:
        print("oups")
    try:
        for keyword in soup.findAll('h1'):
            for word in keyword.text.split(' '):
                list_keywords.append(word)
    except UnicodeEncodeError:
        print("oups")
    try:
        for keyword in soup.findAll('strong'):
            for word in keyword.text.split(' '):
                list_keywords.append(word)
    except UnicodeEncodeError:
        print("oups")
    try:
        for keyword in soup.findAll('h2'):
            for word in keyword.text.split(' '):
                list_keywords.append(word)
    except UnicodeEncodeError:
        print("oups")
    try:
        str_desc = soup.find("div").text
        str_desc.replace("\n","")
        dico["description"] = [str_desc[:250]]
    except UnicodeEncodeError:
        print("oups")
    except AttributeError:
        print("oups")

    dico["title"] = soup.find('title')
    dico["links"] = list_link
    dico["keywords"] = list(set(list_keywords))
    return dico

def outside(url, depth = 2):
    i = 0
    list_url = []
    page = urlopen(url)
    soup = BeautifulSoup.BeautifulSoup(page)
    result = extract(soup)
    for link in result["links"]:
        list_url.append(link)
    while i < depth:
        i += 1
        for link in list_url:
            try:
                page = urlopen(link)
                soup = BeautifulSoup.BeautifulSoup(page)
            except TimeoutError:
                print("TimeoutError\n")
            result = extract(soup)
            for link in result["links"]:
                list_url.append(link)
    return list_link		

def inside(url, depth):
    i = 1
    list_url = []
    list_url_final = []
    page = urlopen(url)
    soup = BeautifulSoup.BeautifulSoup(page)
    result = extract_link(soup)
    list_tmp = list(set(result["links"]))
    for link in list_tmp:
        list_url.append(link)
        list_url_final.append(link)
    while i < int(depth):
        i = i+1
        for link in list_url:
            try:
                page = urlopen(link)
                soup = BeautifulSoup.BeautifulSoup(page)
            except TimeoutError:
                print("TimeoutError\n")
            except urllib.error.HTTPError:
                break
            result = extract_link(soup)
            list_tmp = list(set(result["links"]))
            list_url = []
            for link in list_tmp:
                list_url_final.append(link)
                list_url.append(link)
    dico["links"] = list(set(list_url_final))
    return dico
