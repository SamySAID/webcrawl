""""
Main du projet WebCrawler
auteur:
Samy SAIDANI, Vartan KOKO
"""
from WebCrawl import WebCrawl
import argparse
import sys

if __name__ == "__main__":

    sys.setrecursionlimit(100000)
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument("-c", "--crawl", type=str, help="Crawl de l'URL")
    PARSER.add_argument("-e", "--extract", type=str, help="Extraire info de l'URL")
    PARSER.add_argument("-d", "--depth_outside", type=int, help="Profondeur du crawl")
    PARSER.add_argument("-g", "--go_outside", action="store_true", help="Sortie du site")
    PARSER.add_argument("-s", "--show_dico", action="store_true", help="Contenu dico")

    #Récupération des options passé en parametre par l'utilisateurs.
    ARGS = PARSER.parse_args()

    CRAWLER = WebCrawl(ARGS.crawl, ARGS.extract, ARGS.depth_outside, ARGS.go_outside)

    if ARGS.show_dico:
        CRAWLER.show_dico()
