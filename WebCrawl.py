""""
Classe WebCrawl du projet WebCrawler
"""

import pickle
import sys, io
from urllib.request import urlopen
import urllib.error
import bs4 as BeautifulSoup
import fonction

class WebCrawl:
    """
    Classe principale
    """
    result = dict()
    depth = 0
    dictionnaire = dict()

    def __init__(self, crawl, extract, depth_outside, go_outside):
        sys.stdout = io.TextIOWrapper(sys.stdout.buffer, 'cp437', 'backslashreplace')

        if extract:
            file_out = open('dico', 'wb')
            pick = pickle.Pickler(file_out)
            pick.dump(self.extract_from_link(extract))


        if crawl:
            dicoTmp = dict()
            file_out = open('dico', 'wb')
            pick = pickle.Pickler(file_out)
            self.depth = depth_outside
            if self.depth == 0:
                file_out = open('dico', 'wb')
                pick = pickle.Pickler(file_out)
                pick.dump(self.extract_from_link(crawl))
                sys.exit(0) 
            if go_outside:
                self.result = fonction.inside(crawl, self.depth)
                for link in self.result["links"]:
                    dicoTmp = self.extract_from_link(link)
                    if link in dicoTmp.keys():
                        print(dicoTmp[link]['title'])
                        self.dictionnaire[link] = dict()
                        self.dictionnaire[link]['title'] = dicoTmp[link]['title']
                        self.dictionnaire[link]['links'] = dicoTmp[link]['links'].copy()
                        self.dictionnaire[link]['keywords'] = dicoTmp[link]['keywords'].copy()
                        self.dictionnaire[link]['description'] = dicoTmp[link]['description']
                        dicoTmp[link]['links'][:] = []
                        dicoTmp[link]['keywords'][:] = []
            else:
                self.result = fonction.inside(crawl, self.depth)
            for link in self.result["links"]:
                if crawl in link:
                    dicoTmp = self.extract_from_link(link)
                    if link in dicoTmp.keys():
                        print(dicoTmp[link]['title'])
                        self.dictionnaire[link] = dict()
                        self.dictionnaire[link]['title'] = dicoTmp[link]['title']
                        self.dictionnaire[link]['links'] = dicoTmp[link]['links'].copy()
                        self.dictionnaire[link]['keywords'] = dicoTmp[link]['keywords'].copy()
                        self.dictionnaire[link]['description'] = dicoTmp[link]['description']
                        dicoTmp[link]['links'][:] = []
                        dicoTmp[link]['keywords'][:] = []
            pick.dump(self.dictionnaire)
    
    def extract_from_link(self,link):
        dictionnaire = dict()
        try:
            page = urlopen(link)
        except urllib.error.HTTPError:
            return dictionnaire
        soup = BeautifulSoup.BeautifulSoup(page)
        dictionnaire[link] = fonction.extract_data(soup)
        return dictionnaire

    def show_dico(self):
        """
        Méthode permettant d'afficher le contenu du dictionnaire
        """
        try:
            with open('dico', 'rb') as file_in:
                depick = pickle.Unpickler(file_in)
                try:
                    self.dictionnaire = depick.load()
                    file_in.close()
                except EOFError:
                    print("\n")
        except FileNotFoundError:
            print("Pas de dictionnaire trouvé")
        try:
            for link in self.dictionnaire:
                print("--------------------------------------------------------------------------------------------------\nlink : ",
                link,"\n------------------------------------------------------------------------------------------------------------------------")
                for cat in self.dictionnaire[link]:
                    print("------> ",cat," :")
                    for data in self.dictionnaire[link][cat]:
                        print("                 ",data)
        except KeyError:
            print("Pas de dico trouvé")